const christmasDate = new Date('25 Dec 2022');

function setCountdown(){
  const currentDate = new Date();

  var daysElement = document.getElementById('timerDays');
  var hoursElement = document.getElementById('timerHours');
  var minutesElement = document.getElementById('timerMinutes');
  var secondsElement = document.getElementById('timerSeconds');

  const datesDifference = (christmasDate - currentDate) / 1000;

  var days = Math.floor(datesDifference / (24 * 60 * 60));
  var hours = Math.floor(datesDifference / (60 * 60)) % 24;
  var minutes = Math.floor(datesDifference / 60) % 60;
  var seconds = Math.floor(datesDifference) % 60;

  daysElement.innerHTML = days;
  hoursElement.innerHTML = hours;
  minutesElement.innerHTML = minutes;
  secondsElement.innerHTML = seconds;
  console.log(datesDifference, Math.floor(datesDifference));
  console.log(datesDifference / (60 * 60), Math.floor(datesDifference / (60 * 60)));
  console.log(datesDifference / (60 * 60) / 24, Math.floor(datesDifference / (60 * 60)) / 24);
  console.log(datesDifference / (60 * 60) % 24, Math.floor(datesDifference / (60 * 60)) % 24);
  console.log(60 * 60);
}

setCountdown();
// setInterval(setCountdown, 1000);


// 1,000 ms == 1 s
// 60,000 ms == 60s == 1m
// 3,600,000 ms == 1m == 1h
